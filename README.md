Simple script to generate brilcalc-like csv output from hd5 files. Designed for Run2 stability checks.

**IMPORTANT**: Currently it does not support correction which might be applied from YAML iovtags, will be added to the next revision, thus the rates should be corrected before final conclusions.

Values are simple mean for LS

To run one should change configuration part (line 119) and specify:
file_path (top directory with fills following brildata structure)

node (e.g. "hfetlumi")

data_tag (optional by default 23v1)

norm_tag (if needed)

source (as will appear in "brilcalc" output)


and sigvis to recalculate raw rates. **IMPORTANT**: the script actually recalculates "avg" value applying collision mask to the raw rates to achieve maximum communality among systems (all using different approaches to calculate orbit integrated rates)


Provides /output for each hd5 file (fill/run) and final combined csv.


One could uncomment line 137 to specify list of fills, alternatively it will iterate over all folders from specified file_path folder
