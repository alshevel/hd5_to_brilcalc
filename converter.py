import tables,sys,os
import pandas as pd
import numpy as np

import glob, pathlib

# ====================================================================
# HD5 extraction MODULE
# ====================================================================

# Read list of Fills. Consider to remove to separate file to reuse for all lumistore
def read_fill(path: str, fill: int, node: str):
    return pd.concat([read_run(f, node) for f in glob.glob(f'{path}/{fill}/*.hd5', recursive=True)])

def read_run(file_path, node):
    with tables.open_file(file_path, 'r') as table:
            data = pd.DataFrame( data=table.root[node][:].tolist()
                                   , columns=table.root[node].colnames)
    table.close()
    
    return data

# ====================================================================
# Main Routines
# ====================================================================

def generate_output_df(df):
     
    output_df = pd.DataFrame()

    output_df['#run:fill'] =  df['runnum'].astype(str) + ':' + df['fillnum'].astype(str)
    #output_df = output_df.set_index('fill:run:ls:nb')

    output_df['ls'] = df['lsnum'].astype(str) + ':' + df['lsnum'].astype(str)
    output_df['time'] = pd.to_datetime(df['timestampsec'], unit='s')
    output_df['time'] = output_df['time'].dt.strftime('%m/%d/%Y %H:%M:%S')

    output_df['beamstatus'] = df['status'].str.decode("utf-8")
    output_df['E(GeV)'] = [energy for x in range(df.shape[0])]

    # Calculate avgraw
    mask = np.stack(df['collidable'])
    bxraw = np.stack(df['bxraw'])

    output_df['delivered(/ub)'] = np.array([np.multiply(bxraw[entry], mask[entry]).sum() for entry in range(len(bxraw))])
    output_df['avgpu'] = output_df['delivered(/ub)']/df['ncollidable'] * 11245.6/sigvis * 8
    
    output_df['delivered(/ub)'] = output_df['delivered(/ub)'] * 11245.6/sigvis * 23.2
    output_df['recorded(/ub)'] = output_df['delivered(/ub)']

    output_df['source'] = [source for x in range(df.shape[0])]

    # Inelegant solution to calculate mean numbers and save df's structure
    df_to_concat = []
    for run_fill in output_df['#run:fill'].unique():
        run_df = output_df.loc[(output_df['#run:fill'] ==  run_fill)]
        for ls in run_df['ls'].unique():
            ls_df = run_df.loc[(run_df['ls'] ==  ls)]
             
            mean_delivered = ls_df['delivered(/ub)'].mean()
            mean_pileup = ls_df['avgpu'].mean()
            
            ls_df = ls_df.drop_duplicates(subset=['#run:fill'], keep='first')
            ls_df['delivered(/ub)'] = mean_delivered
            ls_df['avgpu'] = mean_pileup

            df_to_concat.append(ls_df)

    output_df = pd.concat([loc_df for loc_df in df_to_concat])
    # To match with brilcalc
    output_df = output_df.round({'avgpu': 1})

    return output_df


# Process HD5 Files and generate csv brilcalc-like output for each run
def generate_files(fills_list):

    for fill in fills_list:
        
        output_path = f'output/{source}/{data_tag}/{fill}/'
        if not os.path.isdir(output_path):
            os.makedirs(output_path)

        for path in pathlib.Path(f'{file_path}{fill}/').glob('*.hd5'):

            try:

                # Read beam and lumi data
                beam = read_run(path, 'beam')
                lumi = read_run(path, node)
            
                # Align beam data and luminosity
                beam_slice = beam[['fillnum', 'runnum', 'lsnum', 'nbnum', 'status', 'collidable', 'ncollidable']]
                df = pd.merge( lumi
                            , beam_slice
                            , on = [ 'fillnum', 'runnum', 'lsnum', 'nbnum'])
                
                # Assume that files were already sorted
                run = df['runnum'].unique()[0]

                output_df = generate_output_df(df)
                print(output_df)

                file_name = f'{output_path}/{fill}_{run}_{node}.csv'

                output_df.to_csv(file_name, index=False)
            except:
                print(f'Failed to process file: {path}')

def combine(source, data_tag):

    # Read all files in folder
    df = pd.concat([pd.read_csv(f) for f in glob.glob(f'output/{source}/{data_tag}/*/*.csv', recursive=True)], ignore_index=True)
    df.to_csv(f'{source}_{data_tag}.csv', index=False)


# ====================================================================
# Configuration part. TODO: Move to external config
# ====================================================================
file_path = '/brildata/23/'
node = 'hfetlumi'
data_tag = '23v1'
norm_tag = 'None'
energy = 6800
source = 'HFET'

sigvis = 3263.33
# TODO: Add corrections picked up from iov YAML files to apply efficiency corrections
# ...

fills_list = [int(x.replace(file_path, '')) for x in glob.iglob(f'{file_path}/*', recursive = True)]
fills_list.sort()

# Or provide number of fills
fills_list = [9036]

generate_files(fills_list)

# Generate combined csv for specific detector. Supposed to be used separately
combine(source, data_tag)